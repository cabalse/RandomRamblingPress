import React from 'react';
import { Row, Col, Image } from '../bootstrap/Bootstrap';
import { useQuery, gql } from '@apollo/client';

import './Products.scss';

import { product_image_path } from '../../Config';
import ProductInformation from './ProductInformation';
import ProductLink from './ProductLink';
import { FormatString } from './stringFormatHelper';

const PRODUCTS = gql`
  query {
    products {
      id
      date
      image
      title
      intro
      description
      type
      format
      pages
      status
      price
      linktext
      link
    }
  }
`;

const Products = () => {
  const { loading, error, data } = useQuery(PRODUCTS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return data.products.map((item) => (
    <Row key={item.id} className="product m-1 pt-3 pb-3">
      <Col className="product-image col-auto">
        <Image src={product_image_path + item.image} />
      </Col>
      <Col className="product-text justify-content-start">
        <h1>{item.title}</h1>
        <p>
          <i>{FormatString(item.intro)}</i>
        </p>
        <p>{FormatString(item.description)}</p>
        <ProductInformation item={item} />
        <ProductLink linkText={item.linktext} link={item.link} />
      </Col>
    </Row>
  ));
};

export default Products;
